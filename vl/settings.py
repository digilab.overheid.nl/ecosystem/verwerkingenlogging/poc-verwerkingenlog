import os
import sys
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent

def get_env(key, default_value):
    e = os.environ.get(key, default_value)

    if e == 'true':
        return True
    if e == 'false':
        return False

    return e


DEBUG                       = get_env('DEBUG', False)
SECRET_KEY                  = get_env(
    'SECRET_KEY',
    'django-insecure-0102030405060708090a0b0c0d0e0f10111213141516171819',
)
ALLOWED_HOSTS               = get_env('ALLOWED_HOSTS', '*').split(',')
CSRF_TRUSTED_ORIGINS        = []
SECURE_PROXY_SSL_HEADER     = ('HTTP_X_FORWARDED_PROTO', 'https')

ROOT_URLCONF                = 'vl.urls'
WSGI_APPLICATION            = 'vl.wsgi.application'
STATIC_URL                  = os.getenv('STATIC_URL', '/static/')
STATIC_ROOT                 = os.path.join(BASE_DIR, 'static')

TIME_ZONE                   = 'UTC'
USE_I18N                    = True
USE_L10N                    = True
USE_TZ                      = True
LANGUAGE_CODE               = 'en'

# See: https://code.djangoproject.com/ticket/32577.
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
if get_env('DB_NAME', False):
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'HOST': get_env('DB_HOST', 'localhost'),
            'NAME': os.getenv('DB_NAME', 'vl'),
            'USER': get_env('DB_USER', 'vl'),
            'PASSWORD': get_env('DB_PASSWORD', 'vl'),
            'CONN_MAX_AGE': None,
            'CONN_HEALTH_CHECKS': True,
            'OPTIONS': {
                'keepalives': "1",
                'keepalives_idle': "120",
                'keepalives_interval': "20",
            }
        },
    }

FORCE_SCRIPT_NAME = os.getenv("FORCE_SCRIPT_NAME", None)
WHITENOISE_STATIC_PREFIX = '/static/'  # See: https://github.com/evansd/whitenoise/issues/271.

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'rest_framework',
    'rest_framework.authtoken',
    'drf_spectacular',
    'django_filters',

    'vl.apps.django_drf_adr',  # TODO: Refactor into standalone app with a separate repository?
    'vl.apps.api',
    'vl.apps.frontend',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',

    # Needs to be placed after SecurityMiddlware.
    "whitenoise.middleware.WhiteNoiseMiddleware",

    # Needs to be placed before CommonMiddleware.
    'vl.apps.django_drf_adr.middleware.HeaderMiddleware',

    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'stdout': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'stream': sys.stdout,
        },
        'stderr': {
            'class': 'logging.StreamHandler',
            'level': 'ERROR',
            'stream': sys.stderr,
        },
    },
    'loggers': {
        'django': {
            'handlers': ['stdout', 'stderr'],
        },
    },
}

#####################
# App specifics.

API_VERSION_SEMVER = '0.0.1'
API_VERSION = 'v0'

REST_FRAMEWORK = {
    'DEFAULT_SCHEMA_CLASS': 'drf_spectacular.openapi.AutoSchema',
    'DEFAULT_FILTER_BACKENDS': ['django_filters.rest_framework.DjangoFilterBackend'],
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 100,
}

# API Design Rule: API-51: Publish OAS document at a standard location in JSON-format.
SPECTACULAR_SETTINGS = {
    'TITLE': 'PoC Verwerkingenlog',
    'DESCRIPTION': 'OpenAPI spec van de PoC VerwerkingenLog Referentie Implementatie',
    'VERSION': '1.0.0',
    'SERVE_INCLUDE_SCHEMA': False,
    'SCHEMA_PATH_PREFIX': '/api/v\d+/',
}
