import sys
import uuid

from django.db import models
from django.db.models.signals import pre_save


class VerwerkingsActie(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, unique=True)
    created = models.DateTimeField(auto_now_add=True)

    # De link met Register van de Verwerkingsactiviteiten. Indien niet opgegeven: soft fail.
    # Opbouw: https://datastelsel.nl/VAR/OIN/UUID, waarbij de UUID verwijst naar de verwerkings_activiteit_id
    # in het VerwerkingsActiviteit model van de betreffende VAR.
    verwerkings_activiteit = models.URLField(blank=True, null=True)

    # Groepeert bij elkaar horende acties. Indien niet opgegeven wordt een nieuwe gemaakt.
    verwerkings_span = models.UUIDField(default=uuid.uuid4)

    systeem = models.CharField(max_length=255, blank=False, null=False)  # Voorkeur: URI (verwijzing), naief: string.
    verwerker = models.CharField(max_length=255, blank=False, null=False)  # Voorkeur: URI (verwijzing), naief: string.
    verantwoordelijke = models.CharField(max_length=255, blank=False, null=False)  # Voorkeur: URI (verwijzing), naief: string.
    transaction_id = models.UUIDField(blank=True, null=True)

    @staticmethod
    def pre_save(sender, instance, **kwargs):
        # Wanneer een verwerkingsactie geen verwerking_activiteit meekrijgt,
        # zoals verkregen vanuit het Register van de Verwerkingsactiviteiten,
        # betekent dat een blinde vlek: er is nog geen bekende activiteit om deze
        # verwerking actie aan op te hangen.
        # In eerste aanleg reageren we met een 'soft fail': toch opslaan met een
        # fixed handeling type uuid, waarop gefiltert kan worden om het Register
        # mee aan te vullen.
        if instance.verwerkings_activiteit is None:
            print('SOFT FAIL: VerwerkingsActie zonder verwerkings_activiteit', file=sys.stderr)


pre_save.connect(VerwerkingsActie.pre_save, VerwerkingsActie, dispatch_uid="vl.apps.api.models.VerwerkingsActie")
