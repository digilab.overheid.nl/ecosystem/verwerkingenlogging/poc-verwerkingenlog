from django.contrib import admin

from vl.apps.api.models import VerwerkingsActie


class BlindeVlekFilter(admin.SimpleListFilter):
    title = 'Blinde vlek'
    parameter_name = 'verwerkings_activiteit'

    def lookups(self, request, model_admin):
        return (
            (True, 'verwerkings_activiteit ontbreekt'),
        )

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(verwerkings_activiteit=None)


class VerwerkingsActieAdmin(admin.ModelAdmin):
    list_display = ('created', 'verwerkings_activiteit', 'verwerkings_span', 'systeem', 'verantwoordelijke', 'verwerker', 'transaction_id', )
    list_filter = (
        BlindeVlekFilter,
        'verwerkings_span',
    )
    ordering = ('-created', )


admin.site.register(VerwerkingsActie, VerwerkingsActieAdmin)
