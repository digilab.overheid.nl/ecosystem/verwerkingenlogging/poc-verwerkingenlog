from django.db import models

from django_filters import rest_framework as filter
from rest_framework import viewsets

from vl.apps.api.models import VerwerkingsActie
from vl.apps.api.serializers import VerwerkingsActieSerializer


class VerwerkingsActieFilter(filter.FilterSet):

    class Meta:
        model = VerwerkingsActie
        fields = {
            'created': ('date', 'lte', 'gte'),
            'verwerkings_activiteit': ('iexact', 'isnull'),
            'verwerkings_span': ('iexact', ),
        }

    filter_overrides = {
        models.DateTimeField: {
            'filter_class': filter.IsoDateTimeFilter
        },
    }


class VerwerkingsActieViewSet(viewsets.ModelViewSet):
    queryset = VerwerkingsActie.objects.all()
    serializer_class = VerwerkingsActieSerializer
    filterset_class = VerwerkingsActieFilter
