from rest_framework import serializers

from vl.apps.api.models import VerwerkingsActie


class VerwerkingsActieSerializer(serializers.ModelSerializer):

    class Meta:
        model = VerwerkingsActie
        fields = ('created', 'verwerkings_activiteit', 'verwerkings_span', 'systeem', 'verantwoordelijke', 'verwerker', 'transaction_id', )
