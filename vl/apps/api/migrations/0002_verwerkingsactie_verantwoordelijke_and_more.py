# Generated by Django 4.2.5 on 2023-11-21 14:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='verwerkingsactie',
            name='verantwoordelijke',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='verwerkingsactie',
            name='verwerker',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
    ]
