from django.conf import settings
from django.urls import (
    include,
    path,
)

from drf_spectacular.views import (
    SpectacularJSONAPIView,
    SpectacularYAMLAPIView,
)

from vl.apps.api import views
from vl.apps.django_drf_adr.routers import SimpleRouterADR


router = SimpleRouterADR()
router.register(f'{settings.API_VERSION}/verwerkingsactie/', views.VerwerkingsActieViewSet)

urlpatterns = (
    path('', include(router.urls)),

    path(f'{settings.API_VERSION}/openapi.json', SpectacularJSONAPIView.as_view(), name='openapi-schema-json'),
    path(f'{settings.API_VERSION}/openapi.yaml', SpectacularYAMLAPIView.as_view(), name='openapi-schema-yaml'),
)
