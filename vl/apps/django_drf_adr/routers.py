from rest_framework.routers import SimpleRouter


class SimpleRouterADR(SimpleRouter):

    # API Design Rule: API-48: Leave off trailing slashes from URIs.
    def __init__(self, trailing_slash=False):
        super().__init__(trailing_slash)
